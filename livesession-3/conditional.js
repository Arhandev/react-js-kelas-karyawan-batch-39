// if (x < 2) {
// 	console.log('masuk ke dalam if');
// } else {
// 	console.log('masuk ke dalam else');
// }

// console.log('diluar if');

// if (x === 5) {
// 	console.log('setara');
// }

// if (x > 2) {
// 	console.log('lebih dari');
// }

var x = 10000;
if (x < 2) {
	console.log('masuk ke dalam if 1');
} else if (x < 5) {
	console.log('masuk ke dalam else if 2');
} else if (x < 7) {
	console.log('masuk ke dalam else if 3');
}

console.log('ini diluar if');
