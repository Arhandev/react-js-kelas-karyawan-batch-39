import React from 'react';
import Article from './Article';

function ArticleContainer({ data, fetchArticles, setEdit }) {
	return (
		<div className="article-container">
			<h1>Preview Artikel</h1>
			{data.map((item, index) => {
				return (
					<Article
						item={item}
						fetchArticles={fetchArticles}
						setEdit={setEdit}
					/>
				);
			})}
		</div>
	);
}

export default ArticleContainer;
