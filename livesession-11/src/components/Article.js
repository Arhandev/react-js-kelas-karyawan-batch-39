import axios from 'axios';
import React from 'react';

function Article({ item, fetchArticles, setEdit }) {
	const onDelete = async () => {
		try {
			const response = await axios.delete(`https://arhandev.xyz/public/api/articles/${item.id}`);
			alert('Berhasil Delete');
			fetchArticles();
		} catch (error) {
			console.log(error);
			alert('Terjadi Suatu Error');
		}
	};

	const handleEdit = () => {
		setEdit(item);
	};
	return (
		<div className="article">
			<div className="image-container">
				<img src={item.image_url} alt="" />
			</div>
			<div className="content-container">
				<h1>{item.judul}</h1>
				<p>{item.konten}</p>
			</div>
			<div>
				<button onClick={handleEdit}>Edit</button>
				<button onClick={onDelete}>Delete</button>
			</div>
		</div>
	);
}

export default Article;
