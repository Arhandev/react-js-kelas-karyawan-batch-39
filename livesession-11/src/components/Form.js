import axios from 'axios';
import React, { useState } from 'react';

function Form({ fetchArticles }) {
	const [input, setInput] = useState({
		judul: '',
		konten: '',
		image_url: '',
	});
	const [loading, setLoading] = useState(false);

	const onChangeInput = event => {
		setInput({ ...input, [event.target.name]: event.target.value });
	};

	const onSubmit = async () => {
		setLoading(true);
		try {
			const response = await axios.post('https://arhandev.xyz/public/api/articles', {
				judul: input.judul,
				konten: input.konten,
				image_url: input.image_url,
			});
			alert('Berhasil membuat Artikel');
			fetchArticles();
			setInput({
				judul: '',
				konten: '',
				image_url: '',
			});
		} catch (error) {
			console.log(error);
			alert(error.response.data.info);
		} finally {
			setLoading(false);
		}
	};

	return (
		<div className="form">
			<h2 style={{ textAlign: 'center' }}>Create Form</h2>
			<form className="form-input">
				<div className="form-input-group">
					<label>Judul:</label>
					<input type="text" value={input.judul} onChange={onChangeInput} name="judul" placeholder="Masukkan judul" />
				</div>
				<div className="form-input-group">
					<label>Konten:</label>
					<input
						type="text"
						value={input.konten}
						placeholder="Masukkan konten"
						onChange={onChangeInput}
						name="konten"
					/>
				</div>
				<div className="form-input-group">
					<label>Image Url:</label>
					<input
						type="text"
						value={input.image_url}
						placeholder="Masukkan link gambar"
						onChange={onChangeInput}
						name="image_url"
					/>
				</div>
				<div className="button-container">
					<button onClick={onSubmit} disabled={loading} type="button">
						{loading === true ? 'Loading...' : 'Simpan'}
					</button>
				</div>
			</form>
		</div>
	);
}

export default Form;
