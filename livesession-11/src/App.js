import axios from 'axios';
import { useEffect, useState } from 'react';
import './App.css';
import ArticleContainer from './components/ArticleContainer';
import Form from './components/Form';
import UpdateForm from './components/UpdateForm';

function App() {
	const [data, setData] = useState([]);
	const [loading, setLoading] = useState(false);
	const [edit, setEdit] = useState(false);

	const fetchArticles = async () => {
		setLoading(true);
		try {
			const response = await axios.get('https://arhandev.xyz/public/api/articles');
			setData(response.data.data);
			setLoading(false);
		} catch (error) {
			console.log(error);
			setLoading(false);
		}
	};

	useEffect(() => {
		fetchArticles();
	}, []);

	return (
		<div>
			{edit === false ? (
				<Form fetchArticles={fetchArticles} />
			) : (
				<UpdateForm edit={edit} fetchArticles={fetchArticles} setEdit={setEdit} />
			)}
			{loading === true ? (
				<h1 style={{ textAlign: 'center' }}>Loading....</h1>
			) : (
				<ArticleContainer data={data} fetchArticles={fetchArticles} setEdit={setEdit} />
			)}
		</div>
	);
}

export default App;
