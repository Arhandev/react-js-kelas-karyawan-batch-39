// var age = 15;
// console.log("Halo umurku " + age)
// console.log(`Halo umurku ${age}`)

// function testFunction() {
// 	console.log('Halo ini dari function standar');
// }

// const testArrowFunction = () => {
// 	console.log('Halo ini dari Arrow Function');
// };

// testFunction()
// testArrowFunction();

// ====================
// let arr = ['kucing', 'kambing', 'sapi', 'domba'];

// let kalimat = '';

// for (result of arr) {
// 	console.log(result);
// 	kalimat += `${result} `;
// }
// console.log(kalimat);

// ========================

// let arr = ['kambing', 'kucing', 'sapi'];
// let [kambingVar, kucingVar, sapiVar, ularVar] = arr;

// console.log(ularVar);

// ========================

// let obj = {
// 	nama: 'farhan',
// 	domisili: 'jakarta',
// 	handphone: '098903123',
// };

// let { nama, domisili, handphone, status } = obj;
// console.log(domisili);

// ========================

// let arr = ['kambing', 'kucing', 'sapi', 'ular'];
// let [kambingVar, kucingVar, ...sisa] = arr;

// console.log(sisa);

// ========================

// let obj = {
// 	nama: 'farhan',
// 	domisili: 'jakarta',
// 	handphone: '098903123',
//     status: "belajar"
// };

// let { nama, domisili, ...sisa } = obj;
// console.log(sisa);

// ========================

// const arrowFunction = (...rest) => {
// 	console.log(rest[2]);
// };

// arrowFunction(100, 200, 300);

// ========================

// let arr1 = [10, 20, 30, 40]
// let arr2 = [50, 60, 70, 80]
// let arrTotal = [...arr2, ...arr1]
// console.log(arrTotal);

// ========================

// const array = [5, 7, 8, 14, 12];
// const multipleArr = array.map((value)=>{
//     console.log(value)
//     return value * 2
// })

// console.log("========== end loop ==============")
// console.log(multipleArr);

// ===============================

// const array = [5, 7, 8, 14, 12];
// let total = 0;
// array.forEach(value => {
// 	total += value;
// });
// console.log(total);

// ===============================

// const array = [20, 30, 15, 25];
// const filter = array.filter((value)=>{
//     return value > 20
// })
// console.log(filter);

// ===============================

// let array = [1, 2, 100, 10, 12, 20, 15];
// array.sort((a, b) => {
// 	return b - a;
// });
// console.log(array);

// function volumeTabung(r, tinggi) {
// 	return luasLingkaran(r) * tinggi;
// }

// function luasLingkaran(r) {
// 	return 3.14 * r * r;
// }

// var result = volumeTabung(5, 10);
// console.log(result);

// ===============================

// let x = 10;

// if (true) {
// 	x = 20;
// 	console.log(x);
// }

// console.log(x);

let x = 10;

const arr = [10, 15, 12, 18, 21];
const doubleOddArr = arr.map((value, index) => {
	console.log(index + 1);
	if (index % 2 === 1) {
		return value * 2;
	} else {
		return value;
	}
});
console.log(doubleOddArr);
