import { Layout, Menu } from 'antd';
import React, { useState } from 'react';
const { Header, Sider, Content } = Layout;

const items1 = ['1', '2', '3'].map(key => ({
	key,
	label: `nav ${key}`,
}));
const AntdTest = () => {
	const [collapsed, setCollapsed] = useState(false);
	return (
		<Layout className="h-screen">
			<Header>
				<div className="logo" />
				<Menu theme="dark" mode="horizontal" defaultSelectedKeys={['2']} items={items1} />
			</Header>
		</Layout>
	);
};
export default AntdTest;
