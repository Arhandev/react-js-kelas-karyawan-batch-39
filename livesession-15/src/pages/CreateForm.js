import React from 'react';
import Form from '../components/Form';
import Navbar from '../components/Navbar';

function CreateForm() {
	return (
		<div>
			<Navbar />
			<Form />
		</div>
	);
}

export default CreateForm;
