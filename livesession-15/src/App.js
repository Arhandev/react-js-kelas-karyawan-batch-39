import React from 'react';
import { createBrowserRouter, RouterProvider } from 'react-router-dom';
import './App.css';
import AntdTest from './pages/AntdTest';
import CreateForm from './pages/CreateForm';
import Home from './pages/Home';
import TablePage from './pages/Table';
import TailwindTest from './pages/TailwindTest';
import UpdatePage from './pages/UpdatePage';

const router = createBrowserRouter([
	{
		path: '/',
		element: <Home />,
	},
	{
		path: '/table',
		element: <TablePage />,
	},
	{
		path: '/create',
		element: <CreateForm />,
	},
	{
		path: '/edit/:id',
		element: <UpdatePage />,
	},
	{
		path: '/test-tailwind',
		element: <TailwindTest />,
	},
	{
		path: '/test-antd',
		element: <AntdTest />,
	},
]);

function App() {
	return (
		<div>
			<RouterProvider router={router} />
		</div>
	);
}

export default App;
