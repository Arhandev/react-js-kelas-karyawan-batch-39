import React from 'react';

function Article({ item }) {
	return (
		<div className="flex gap-5 p-3 border-2 border-solid border-gray-600">
			<div className="w-1/5">
				<img src={item.image_url} className="w-full" alt="" />
			</div>
			<div className="w-4/5">
				<h1>{item.judul}</h1>
				<p>{item.konten}</p>
			</div>
		</div>
	);
}

export default Article;
