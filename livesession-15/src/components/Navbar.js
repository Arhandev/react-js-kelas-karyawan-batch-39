import React from 'react';
import { Link } from 'react-router-dom';

function Navbar() {
	return (
		<div className="bg-black py-3 px-8 flex items-center gap-5 font-bold">
			<Link to="/">
				<p className="text-white no-underline">Preview</p>
			</Link>
			<Link to="/table">
				<p className="text-white no-underline">Table</p>
			</Link>
		</div>
	);
}

export default Navbar;
