import axios from 'axios';
import { Formik } from 'formik';
import React, { useEffect, useState } from 'react';
import { useNavigate, useParams } from 'react-router-dom';
import * as Yup from 'yup';

const validationSchema = Yup.object().shape({
	judul: Yup.string().required('Judul wajib diisi'),
	konten: Yup.number().typeError('Harus Berupa Angka').required('Konten wajib diisi'),
	image_url: Yup.string().required('Image wajib diisi').url(),
});

function UpdateForm() {
	const initialState = {
		konten: '',
		image_url: '',
		judul: '',
	};

	const { id } = useParams();
	const navigate = useNavigate();
	const [loading, setLoading] = useState(false);
	const [loadingAll, setLoadingAll] = useState(false);

	const [input, setInput] = useState(initialState);

	useEffect(() => {
		fetchArticleDetail();
	}, []);

	const onUpdate = async values => {
		setLoading(true);
		try {
			const response = await axios.put(`https://arhandev.xyz/public/api/articles/${id}`, {
				judul: values.judul,
				konten: values.konten,
				image_url: values.image_url,
			});
			alert('Berhasil update');
			navigate('/table');
		} catch (error) {
			console.log(error);
			alert('Terjadi Kesalahan');
		} finally {
			setLoading(false);
		}
	};

	const fetchArticleDetail = async () => {
		setLoadingAll(true);
		try {
			const response = await axios.get(`https://arhandev.xyz/public/api/articles/${id}`);
			setInput({
				judul: response.data.data.judul,
				konten: response.data.data.konten,
				image_url: response.data.data.image_url,
			});
		} catch (error) {
			console.log(error);
		} finally {
			setLoadingAll(false);
		}
	};

	return loadingAll ? (
		<div style={{ textAlign: 'center', fontSize: '40px', margin: '20px' }}>Loading.....</div>
	) : (
		<div className="form">
			<h2 style={{ textAlign: 'center' }}>Update Form</h2>
			<Formik enableReinitialize onSubmit={onUpdate} initialValues={input} validationSchema={validationSchema}>
				{params => {
					return (
						<form className="form-input">
							<div className="form-input-group">
								<label>Judul:</label>
								<input
									type="text"
									value={params.values.judul}
									onChange={params.handleChange}
									onBlur={params.handleBlur}
									name="judul"
									placeholder="Masukkan judul"
								/>
								<div></div>
								{params.touched.judul && params.errors.judul && (
									<div className="error-message">{params.errors.judul}</div>
								)}
							</div>
							<div className="form-input-group">
								<label>Konten:</label>
								<input
									type="text"
									value={params.values.konten}
									onChange={params.handleChange}
									onBlur={params.handleBlur}
									name="konten"
									placeholder="Masukkan konten"
								/>
								<div></div>
								{params.touched.konten && params.errors.konten && (
									<div className="error-message">{params.errors.konten}</div>
								)}
							</div>
							<div className="form-input-group">
								<label>Image Url:</label>
								<input
									type="text"
									value={params.values.image_url}
									onChange={params.handleChange}
									onBlur={params.handleBlur}
									name="image_url"
									placeholder="Masukkan link gambar"
								/>
								<div></div>
								{params.touched.image_url && params.errors.image_url && (
									<div className="error-message">{params.errors.image_url}</div>
								)}
							</div>
							<div className="button-container">
								<button
									className={params.isValid ? 'button-submit' : 'button-disabled'}
									onClick={params.handleSubmit}
									disabled={loading}
									type="button"
								>
									{loading === true ? 'Loading...' : 'Update'}
								</button>
							</div>
						</form>
					);
				}}
			</Formik>
		</div>
	);
}

export default UpdateForm;
