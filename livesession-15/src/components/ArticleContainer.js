import React, { useContext, useEffect } from 'react';
import { GlobalContext } from '../context/GlobalContext';
import Article from './Article';

function ArticleContainer() {
	const { data, fetchArticles, loading } = useContext(GlobalContext);

	useEffect(() => {
		fetchArticles();
	}, []);

	return loading ? (
		<div className="text-center text-4xl m-5">Loading.....</div>
	) : (
		<div className="max-w-3xl m-auto flex flex-col gap-3">
			<h1 className="text-2xl font-bold my-4">Preview Artikel</h1>
			{data.map((item, index) => {
				return <Article item={item} />;
			})}
		</div>
	);
}

export default ArticleContainer;
