import React, { useEffect } from 'react';

function Block() {
	useEffect(() => {
		console.log('Mounting Block component');
		return () => {
			console.log('Unmounting Black Component');
		};
	}, []);
	return (
		<div
			style={{ border: '2px solid black', backgroundColor: 'red', color: 'white', fontWeight: 'bold', padding: '20px' }}
		>
			<h1>Halo ini dari Block Component</h1>
		</div>
	);
}

export default Block;
