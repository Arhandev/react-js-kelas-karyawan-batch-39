import React, { useEffect, useState } from 'react';
import './App.css';
import Block from './Block';

function App() {
	const [counter, setCounter] = useState(15);
	const [kalimat, setKalimat] = useState('');
	const [show, setShow] = useState(false);
	// const [quantity, setQuantity] = useState(0);
	// const [harga, setHarga] = useState(0);

	// let count = 0;

	const onMinus = () => {
		// count = count - 1;
		// setQuantity(quantity - 1);
		// setHarga(50000 * (quantity - 1));
		if (counter > 0) {
			setCounter(counter - 1);
		}
	};
	const onPlus = () => {
		// count = count + 1;
		// setQuantity(quantity + 1);
		// setHarga(50000 * (quantity + 1));
		setCounter(counter + 1);
	};

	// ===== useEffect saat mounting ====
	// useEffect(() => {
	// 	setCounter(10)
	// }, []);

	useEffect(() => {
		if (counter < 18) {
			setKalimat('Kamu belum cukup umur');
		} else {
			setKalimat('Kamu sudah dewasa');
		}
		// counter < 18 ? setKalimat('Kamu belum cukup umur') : setKalimat('Kamu sudah dewasa');
	}, [counter]);

	const onClickButton = () => {
		setShow(!show);
	};

	return (
		<div className="container">
			{/* <div>
				<h1>{counter < 18 ? 'Kamu belum cukup umur' : 'Kamu sudah dewasa'}</h1>
				<h1>{kalimat}</h1>
			</div> */}
			{/* <div className="App">
				<button onClick={onMinus}>Minus</button>
				<div></div>
				<h1>{counter}</h1>
				<button onClick={onPlus}>plus</button>
			</div> */}
			{/* <div>
				<h1>Quantity: {quantity}</h1>
				<h1>Harga: {harga}</h1>
			</div> */}

			<h1>Life Cycle Unmounting</h1>
			<button onClick={onClickButton}>Klik</button>
			{show === true && <Block />}
		</div>
	);
}

export default App;
