import './App.css';
import './style.css';
import appStyle from './App.module.css'

function App() {
	return (
		<div className={appStyle.App}>
			<h1 style={{ backgroundColor: 'red' }}>Halo ini dari App.js</h1>
			<h1>Halo ini dari App.js</h1>
		</div>
	);
}

export default App;
