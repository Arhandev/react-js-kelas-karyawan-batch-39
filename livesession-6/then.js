const promiseExample = name => {
	return new Promise((resolve, rejected) => {
		if (name === 'farhan') {
			resolve('Kamu adalah farhan');
		} else {
			rejected('kamu bukan farhan');
		}
	});
};

promiseExample('farhan 123')
	.then(data => {
		console.log(data);
	})
	.catch(error => {
		console.log(error);
	});
