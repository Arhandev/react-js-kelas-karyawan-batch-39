const execPromise = async () => {
    try {
        const result = await promiseExample("hehe");
        console.log(result);
    } catch (error) {
        console.log('ini adalah block catch');
        console.log(error);
    }
};

const promiseExample = age => {
	return new Promise((resolve, rejected) => {
		if (age < 17) {
			resolve('anak-anak');
		} else if (age >= 17 && age < 55) {
			resolve('Dewasa');
		} else if (age >= 55) {
			resolve('Lanjut usia');
		} else {
			rejected('umur tidak valid');
		}
	});
};


execPromise();
