import axios from 'axios';
import { useEffect, useState } from 'react';
import './App.css';
import ArticleContainer from './components/ArticleContainer';
import Form from './components/Form';

function App() {
	const [data, setData] = useState([]);
	const [loading, setLoading] = useState(false);

	const fetchArticles = async () => {
		setLoading(true);
		try {
			const response = await axios.get('https://arhandev.xyz/public/api/articles');
			setData(response.data.data);
			setLoading(false);
		} catch (error) {
			console.log(error);
			setLoading(false);
		}
	};

	useEffect(() => {
		fetchArticles();
	}, []);

	return (
		<div>
			<Form fetchArticles={fetchArticles} />
			{loading === true ? <h1 style={{ textAlign: 'center' }}>Loading....</h1> : <ArticleContainer data={data} />}
		</div>
	);
}

export default App;
