import React from 'react';

function Article(props) {
	return (
		<div className="article">
			<div className="image-container">
				<img src={props.image_url} alt="" />
			</div>
			<div className="content-container">
				<h1>{props.judul}</h1>
				<p>{props.konten}</p>
			</div>
		</div>
	);
}

export default Article;
