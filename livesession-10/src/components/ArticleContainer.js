import axios from 'axios';
import React, { useEffect, useState } from 'react';
import Article from './Article';

function ArticleContainer({data}) {
	
	return (
		<div className="article-container">
			<h1>Preview Artikel</h1>
			{data.map((item, index) => {
				return <Article image_url={item.image_url} judul={item.judul} konten={item.konten} />;
			})}
		</div>
	);
}

export default ArticleContainer;
