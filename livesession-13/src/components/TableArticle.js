import axios from 'axios';
import React, { useContext, useEffect } from 'react';
import { Link } from 'react-router-dom';
import { GlobalContext } from '../context/GlobalContext';

function TableArticle() {
	const { count, data, setEdit, fetchArticles, loading } = useContext(GlobalContext);
	const onDelete = async article => {
		let confirm = window.confirm('Apakah kamu yakin ingin menghapus artikel?');
		if (confirm == true) {
			try {
				const response = await axios.delete(`https://arhandev.xyz/public/api/articles/${article.id}`);
				alert('Berhasil Delete');
				fetchArticles();
			} catch (error) {
				console.log(error);
				alert('Terjadi Suatu Error');
			}
		}
	};

	const handleEdit = article => {
		setEdit(article);
	};

	useEffect(() => {
		fetchArticles();
	}, []);

	return loading ? (
		<div style={{ textAlign: 'center', fontSize: '40px', margin: '20px' }}>Loading.....</div>
	) : (
		<div>
			<h1 style={{ textAlign: 'center' }}>Tabel Pengaturan Artikel</h1>
			<div style={{ width: '800px', margin: 'auto', display: 'flex', justifyContent: 'flex-end' }}>
				<Link to="/create">
					<button
						style={{
							backgroundColor: 'blue',
							color: 'white',
							fontWeight: 'bold',
							padding: '10px 20px',
							borderRadius: '10px',
						}}
					>
						Buat Artikel
					</button>
				</Link>
			</div>
			<table>
				<thead>
					<tr>
						<th>Gambar</th>
						<th>Judul</th>
						<th>Konten</th>
						<th>Action</th>
					</tr>
				</thead>
				<tbody>
					{data.map(article => {
						return (
							<tr>
								<td>
									<img src={article.image_url} width="200" alt="" />
								</td>
								<td>{article.judul}</td>
								<td>{article.konten}</td>
								<td>
									<div className="action">
										<Link to={`/edit/${article.id}`}>
											<button className="btn btn-edit">Edit</button>
										</Link>
										<button onClick={() => onDelete(article)} className="btn btn-delete">
											Delete
										</button>
									</div>
								</td>
							</tr>
						);
					})}
				</tbody>
			</table>
		</div>
	);
}

export default TableArticle;
