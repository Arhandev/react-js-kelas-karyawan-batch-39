import axios from 'axios';
import React, { useEffect, useState } from 'react';
import { useNavigate, useParams } from 'react-router-dom';

function UpdateForm() {
	const { id } = useParams();
	const navigate = useNavigate();
	const [loading, setLoading] = useState(false);
	const [loadingAll, setLoadingAll] = useState(false);

	const [input, setInput] = useState({
		judul: '',
		konten: '',
		image_url: '',
	});
	const onChangeInput = event => {
		setInput({ ...input, [event.target.name]: event.target.value });
	};

	useEffect(() => {
		fetchArticleDetail();
	}, []);

	const onUpdate = async () => {
		setLoading(true);
		try {
			const response = await axios.put(`https://arhandev.xyz/public/api/articles/${id}`, {
				judul: input.judul,
				konten: input.konten,
				image_url: input.image_url,
			});
			alert('Berhasil update');
			setInput({
				judul: '',
				konten: '',
				image_url: '',
			});
			navigate('/table');
		} catch (error) {
			console.log(error);
			alert('Terjadi Kesalahan');
		} finally {
			setLoading(false);
		}
	};

	const fetchArticleDetail = async () => {
		setLoadingAll(true);
		try {
			const response = await axios.get(`https://arhandev.xyz/public/api/articles/${id}`);
			setInput({
				judul: response.data.data.judul,
				konten: response.data.data.konten,
				image_url: response.data.data.image_url,
			});
		} catch (error) {
			console.log(error);
		} finally {
			setLoadingAll(false);
		}
	};

	return loadingAll ? (
		<div style={{ textAlign: 'center', fontSize: '40px', margin: '20px' }}>Loading.....</div>
	) : (
		<div className="form">
			<h2 style={{ textAlign: 'center' }}>Update Form</h2>
			<form className="form-input">
				<div className="form-input-group">
					<label>Judul:</label>
					<input type="text" value={input.judul} onChange={onChangeInput} name="judul" placeholder="Masukkan judul" />
				</div>
				<div className="form-input-group">
					<label>Konten:</label>
					<input
						type="text"
						value={input.konten}
						onChange={onChangeInput}
						placeholder="Masukkan konten"
						name="konten"
					/>
				</div>
				<div className="form-input-group">
					<label>Image Url:</label>
					<input
						type="text"
						value={input.image_url}
						onChange={onChangeInput}
						placeholder="Masukkan link gambar"
						name="image_url"
					/>
				</div>
				<div className="button-container">
					<button type="button" disabled={loading} onClick={onUpdate}>
						{loading === true ? 'Loading...' : 'Update'}
					</button>
				</div>
			</form>
		</div>
	);
}

export default UpdateForm;
