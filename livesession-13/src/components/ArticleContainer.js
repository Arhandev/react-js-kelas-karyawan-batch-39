import React, { useContext, useEffect } from 'react';
import { GlobalContext } from '../context/GlobalContext';
import Article from './Article';

function ArticleContainer() {
	const { data, fetchArticles, loading } = useContext(GlobalContext);

	useEffect(() => {
		fetchArticles();
	}, []);

	return loading ? (
		<div style={{ textAlign: 'center', fontSize: '40px', margin: '20px' }}>Loading.....</div>
	) : (
		<div className="article-container">
			<h1>Preview Artikel</h1>
			{data.map((item, index) => {
				return <Article item={item} />;
			})}
		</div>
	);
}

export default ArticleContainer;
