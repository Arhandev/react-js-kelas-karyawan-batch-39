import React from 'react';
import Navbar from '../components/Navbar';
import TableArticle from '../components/TableArticle';

function TablePage() {
	return (
		<div>
			<Navbar />
			<TableArticle />
		</div>
	);
}

export default TablePage;
