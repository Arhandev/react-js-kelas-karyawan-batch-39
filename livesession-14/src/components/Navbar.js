import React from 'react';
import { Link } from 'react-router-dom';

function Navbar() {
	return (
		<div
			style={{
				backgroundColor: 'black',
				padding: '5px 30px',
				display: 'flex',
				alignItems: 'center',
				gap: '20px',

				fontWeight: 'bold',
			}}
		>
			<Link to="/">
				<p style={{ color: 'white', textDecoration: 'none' }}>Preview</p>
			</Link>
			<Link to="/table">
				<p style={{ color: 'white' }}>Table</p>
			</Link>
		</div>
	);
}

export default Navbar;
