import axios from 'axios';
import { Formik } from 'formik';
import * as Yup from 'yup';
import React, { useContext, useState } from 'react';
import { useNavigate } from 'react-router-dom';
import { GlobalContext } from '../context/GlobalContext';

const validationSchema = Yup.object({
	judul: Yup.string().required('Judul wajib diisi'),
	konten: Yup.string().required('Konten wajib diisi'),
	image_url: Yup.string().required('Image wajib diisi').url(),
});

function Form() {
	const initialState = {
		konten: '',
		image_url: '',
		judul: '',
	};
	const { count } = useContext(GlobalContext);
	const navigate = useNavigate();

	const [loading, setLoading] = useState(false);

	const onSubmit = async values => {
		console.log(values);
		setLoading(true);
		try {
			const response = await axios.post('https://arhandev.xyz/public/api/articles', {
				judul: values.judul,
				konten: values.konten,
				image_url: values.image_url,
			});
			alert('Berhasil membuat Artikel');
			navigate('/table');
		} catch (error) {
			console.log(error);
			alert(error.response.data.info);
		} finally {
			setLoading(false);
		}
	};

	return (
		<div className="form">
			<h2 style={{ textAlign: 'center' }}>Create Form</h2>
			<Formik onSubmit={onSubmit} initialValues={initialState} validationSchema={validationSchema}>
				{params => {
					return (
						<form className="form-input">
							<div className="form-input-group">
								<label>Judul:</label>
								<input
									type="text"
									value={params.values.judul}
									onChange={params.handleChange}
									onBlur={params.handleBlur}
									name="judul"
									placeholder="Masukkan judul"
								/>
								<div></div>
								{params.touched.judul && params.errors.judul && (
									<div className="error-message">{params.errors.judul}</div>
								)}
							</div>
							<div className="form-input-group">
								<label>Konten:</label>
								<input
									type="text"
									placeholder="Masukkan konten"
									value={params.values.konten}
									onChange={params.handleChange}
									onBlur={params.handleBlur}
									name="konten"
								/>
								<div></div>
								{params.touched.konten && params.errors.konten && (
									<div className="error-message">{params.errors.konten}</div>
								)}
							</div>
							<div className="form-input-group">
								<label>Image Url:</label>
								<input
									type="text"
									placeholder="Masukkan link gambar"
									value={params.values.image_url}
									onChange={params.handleChange}
									onBlur={params.handleBlur}
									name="image_url"
								/>
								<div></div>
								{params.touched.image_url && params.errors.image_url && (
									<div className="error-message">{params.errors.image_url}</div>
								)}
							</div>
							<div className="button-container">
								<button
									className={params.isValid && params.dirty ? 'button-submit' : 'button-disabled'}
									onClick={params.handleSubmit}
									disabled={loading}
									type="button"
								>
									{loading === true ? 'Loading...' : 'Simpan'}
								</button>
							</div>
						</form>
					);
				}}
			</Formik>
		</div>
	);
}

export default Form;
