import React from 'react';
import ArticleContainer from '../components/ArticleContainer';
import Navbar from '../components/Navbar';

function Home() {
	return (
		<div>
			<Navbar />
			<ArticleContainer />
		</div>
	);
}

export default Home;
