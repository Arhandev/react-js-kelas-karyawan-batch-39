import axios from 'axios';
import React, { createContext, useState } from 'react';

export const GlobalContext = createContext();

export const GlobalProvider = ({ children }) => {
	const [counter, setCounter] = useState(0);
	const [data, setData] = useState([]);
	const [edit, setEdit] = useState(false);
	const [loading, setLoading] = useState(false);

	const fetchArticles = async () => {
		setLoading(true);
		try {
			const response = await axios.get('https://arhandev.xyz/public/api/articles');
			setData(response.data.data);
		} catch (error) {
			console.log(error);
		} finally {
			setLoading(false);
		}
	};

	return (
		<GlobalContext.Provider
			value={{
				count: counter,
				setCounter: setCounter,
				data: data,
				setData: setData,
				edit: edit,
				setEdit: setEdit,
				fetchArticles: fetchArticles,
				loading: loading,
				setLoading: setLoading,
			}}
		>
			{children}
		</GlobalContext.Provider>
	);
};
