import axios from 'axios';
import React, { useEffect, useState } from 'react';
import Navbar from '../components/Navbar';

function Profile() {
	const [data, setData] = useState({});
	const [loading, setLoading] = useState(false);
	const fetchProfile = async () => {
		setLoading(true);
		try {
			const response = await axios.get('https://arhandev.xyz/public/api/profile', {
				headers: {
					Authorization: `Bearer ${localStorage.getItem('token')}`,
				},
			});
			console.log(response.data);
			setData(response.data.data.user);
		} catch (error) {
			console.log(error);
			alert(error.response.data.info);
		} finally {
			setLoading(false);
		}
	};
	useEffect(() => {
		fetchProfile();
	}, []);

	return loading ? (
		<div className="text-2xl font-bol text-center">Loading</div>
	) : (
		<div>
			<Navbar />
			<div className="max-w-xl mx-auto mt-6 border-2 border-gray-500 rounded-lg p-6">
				<p className="text-center text-2xl font-bold">Profile</p>
				<div className="grid grid-cols-2">
					<p>Nama:</p>
					<p>{data.name}</p>
					<p>email:</p>
					<p>{data.email}</p>
					<p>username:</p>
					<p>{data.username}</p>
				</div>
			</div>
		</div>
	);
}

export default Profile;
