import React from 'react';
import Navbar from '../components/Navbar';
import RegisterForm from '../components/RegisterForm';

function Register() {
	return (
		<div>
			<Navbar />
			<RegisterForm />
		</div>
	);
}

export default Register;
