import React from 'react';
import Navbar from '../components/Navbar';
import UpdateForm from '../components/UpdateForm';

function UpdatePage() {
	return (
		<div>
			<Navbar />
			<UpdateForm />
		</div>
	);
}

export default UpdatePage;
