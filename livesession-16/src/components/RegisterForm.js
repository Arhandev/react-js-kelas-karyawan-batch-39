import axios from 'axios';
import { Formik } from 'formik';
import React, { useState } from 'react';
import { useNavigate } from 'react-router-dom';
import * as Yup from 'yup';

const validationSchema = Yup.object({
	email: Yup.string().required('Email wajib diisi'),
	username: Yup.string().required('Username wajib diisi'),
	name: Yup.string().required('Nama wajib diisi'),
	password: Yup.string().required('Password wajib diisi'),
	password_confirmation: Yup.string().required('Konfirmasi password wajib diisi'),
});

function RegisterForm() {
	const initialState = {
		email: '',
		username: '',
		name: '',
		password: '',
		password_confirmation: '',
	};
	const navigate = useNavigate();

	const [loading, setLoading] = useState(false);

	const onSubmit = async values => {
		setLoading(true);
		try {
			const response = await axios.post('https://arhandev.xyz/public/api/register', {
				email: values.email,
				username: values.username,
				name: values.name,
				password: values.password,
				password_confirmation: values.password_confirmation,
			});
			alert('Berhasil register');
			// navigate('/table');
		} catch (error) {
			console.log(error);
			alert(error.response.data.info);
		} finally {
			setLoading(false);
		}
	};

	return (
		<div className="form">
			<h2 className="text-2xl font-bold mb-6 text-center">Register Form</h2>
			<Formik onSubmit={onSubmit} initialValues={initialState} validationSchema={validationSchema}>
				{params => {
					return (
						<form className="form-input">
							<div className="form-input-group">
								<label>Email:</label>
								<input
									type="text"
									value={params.values.email}
									onChange={params.handleChange}
									onBlur={params.handleBlur}
									name="email"
									placeholder="Masukkan Email"
									className="border-2 border-gray-500 rounded-sm px-2"
								/>
								<div></div>
								{params.touched.email && params.errors.email && (
									<div className="error-message">{params.errors.email}</div>
								)}
							</div>
							<div className="form-input-group">
								<label>Username:</label>
								<input
									type="text"
									value={params.values.username}
									onChange={params.handleChange}
									onBlur={params.handleBlur}
									name="username"
									placeholder="Masukkan Username"
									className="border-2 border-gray-500 rounded-sm px-2"
								/>
								<div></div>
								{params.touched.username && params.errors.username && (
									<div className="error-message">{params.errors.username}</div>
								)}
							</div>
							<div className="form-input-group">
								<label>Nama:</label>
								<input
									type="text"
									value={params.values.name}
									onChange={params.handleChange}
									onBlur={params.handleBlur}
									name="name"
									placeholder="Masukkan Nama"
									className="border-2 border-gray-500 rounded-sm px-2"
								/>
								<div></div>
								{params.touched.name && params.errors.name && <div className="error-message">{params.errors.name}</div>}
							</div>
							<div className="form-input-group">
								<label>Password:</label>
								<input
									type="password"
									value={params.values.password}
									onChange={params.handleChange}
									onBlur={params.handleBlur}
									name="password"
									placeholder="Masukkan Password"
									className="border-2 border-gray-500 rounded-sm px-2"
								/>
								<div></div>
								{params.touched.password && params.errors.password && (
									<div className="error-message">{params.errors.password}</div>
								)}
							</div>
							<div className="form-input-group">
								<label>Konfirmasi Password:</label>
								<input
									type="password"
									value={params.values.password_confirmation}
									onChange={params.handleChange}
									onBlur={params.handleBlur}
									name="password_confirmation"
									placeholder="Masukkan Password"
									className="border-2 border-gray-500 rounded-sm px-2"
								/>
								<div></div>
								{params.touched.password_confirmation && params.errors.password_confirmation && (
									<div className="error-message">{params.errors.password_confirmation}</div>
								)}
							</div>
							<div className="flex justify-center">
								<button
									type="button"
									className={`px-4 py-2 border rounded-md ${
										params.isValid && params.dirty ? 'border-green-200 bg-green-200' : 'border-gray-400 bg-gray-400'
									} `}
									onClick={params.handleSubmit}
									disabled={loading}
								>
									{loading === true ? 'Loading...' : 'Simpan'}
								</button>
							</div>
						</form>
					);
				}}
			</Formik>
		</div>
	);
}

export default RegisterForm;
