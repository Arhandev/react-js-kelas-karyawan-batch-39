import axios from 'axios';
import React from 'react';
import { Link, useNavigate } from 'react-router-dom';

function Navbar() {
	const navigate = useNavigate();
	const onLogout = async () => {
		try {
			const response = await axios.post(
				'https://arhandev.xyz/public/api/logout',
				{},
				{
					headers: {
						Authorization: `Bearer ${localStorage.getItem('token')}`,
					},
				}
			);
			localStorage.removeItem('token');
			localStorage.removeItem('username');
			navigate('/login');
		} catch (error) {
			console.log(error);
			alert(error.response.data.info);
		}
	};
	return (
		<div className="bg-black py-4 ">
			<div className="w-5/6 mx-auto flex items-center justify-between font-bold">
				<div className="flex items-center gap-5 ">
					<Link to="/">
						<p className="text-white no-underline">Preview</p>
					</Link>
					<Link to="/table">
						<p className="text-white no-underline">Table</p>
					</Link>
					<Link to="/profile">
						<p className="text-white no-underline">Profile</p>
					</Link>
				</div>
				{localStorage.getItem('token') !== null ? (
					<div className="flex items-center gap-5 ">
						<p className="text-xl text-white font-bold">{localStorage.getItem('username')}</p>
						<button onClick={onLogout} className="py-2 px-4 rounded-lg bg-red-300 border border-red-300">
							Logout
						</button>
					</div>
				) : (
					<div className="flex items-center gap-5 ">
						<Link to="/register">
							<button className="py-2 px-4 rounded-lg bg-blue-300 border border-blue-300">Register</button>
						</Link>
						<Link to="/login">
							<button className="py-2 px-4 rounded-lg bg-blue-300 border border-blue-300">Login</button>
						</Link>
					</div>
				)}
			</div>
		</div>
	);
}

export default Navbar;
