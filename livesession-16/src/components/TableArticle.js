import axios from 'axios';
import React, { useContext, useEffect } from 'react';
import { Link } from 'react-router-dom';
import { GlobalContext } from '../context/GlobalContext';

function TableArticle() {
	const { count, data, setEdit, fetchArticles, loading } = useContext(GlobalContext);
	const onDelete = async article => {
		let confirm = window.confirm('Apakah kamu yakin ingin menghapus artikel?');
		if (confirm == true) {
			try {
				const response = await axios.delete(`https://arhandev.xyz/public/api/articles/${article.id}`);
				alert('Berhasil Delete');
				fetchArticles();
			} catch (error) {
				console.log(error);
				alert('Terjadi Suatu Error');
			}
		}
	};

	const handleEdit = article => {
		setEdit(article);
	};

	useEffect(() => {
		fetchArticles();
	}, []);

	return loading ? (
		<div className="text-center text-4xl m-5">Loading.....</div>
	) : (
		<div>
			<h1 className="text-center text-3xl font-bold my-4">Tabel Pengaturan Artikel</h1>
			<div className="w-[800px] m-auto flex justify-end">
				<Link to="/create">
					<button className="bg-blue-700 text-white font-bold py-3 px-5 rounded-lg">Buat Artikel</button>
				</Link>
			</div>
			<table className="max-w-[800px] m-auto border-collapse w-full mt-12">
				<thead>
					<tr>
						<th className="py-3 px-0 bg-blue-700 text-white font-bold text-center text-sm border-2 border-solid border-blue-700 p-2">
							Gambar
						</th>
						<th className="py-3 px-0 bg-blue-700 text-white font-bold text-center text-sm border-2 border-solid border-blue-700 p-2">
							Judul
						</th>
						<th className="py-3 px-0 bg-blue-700 text-white font-bold text-center text-sm border-2 border-solid border-blue-700 p-2">
							Konten
						</th>
						<th className="py-3 px-0 bg-blue-700 text-white font-bold text-center text-sm border-2 border-solid border-blue-700 p-2">
							Action
						</th>
					</tr>
				</thead>
				<tbody>
					{data.map(article => {
						return (
							<tr>
								<td className="border-2 border-solid border-blue-700 p-2">
									<img src={article.image_url} width="200" alt="" />
								</td>
								<td className="border-2 border-solid border-blue-700 p-2">{article.judul}</td>
								<td className="border-2 border-solid border-blue-700 p-2">{article.konten}</td>
								<td className="border-2 border-solid border-blue-700 p-2">
									<div className="action">
										<Link to={`/edit/${article.id}`}>
											<button className="btn btn-edit">Edit</button>
										</Link>
										<button onClick={() => onDelete(article)} className="btn btn-delete">
											Delete
										</button>
									</div>
								</td>
							</tr>
						);
					})}
				</tbody>
			</table>
		</div>
	);
}

export default TableArticle;
