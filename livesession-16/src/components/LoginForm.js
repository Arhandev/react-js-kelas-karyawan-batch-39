import axios from 'axios';
import { Formik } from 'formik';
import React, { useState } from 'react';
import { useNavigate } from 'react-router-dom';
import * as Yup from 'yup';

const validationSchema = Yup.object({
	email: Yup.string().required('Email wajib diisi').email('Email tidak valid'),
	password: Yup.string().required('Password wajib diisi'),
});

function LoginForm() {
	const initialState = {
		email: '',
		password: '',
	};
	const navigate = useNavigate();

	const [loading, setLoading] = useState(false);

	const onSubmit = async values => {
		setLoading(true);
		try {
			const response = await axios.post('https://arhandev.xyz/public/api/login', {
				email: values.email,
				password: values.password,
			});
			localStorage.setItem('token', response.data.data.token);
			localStorage.setItem('username', response.data.data.user.username);
			alert('Berhasil Login');
			navigate('/table');
		} catch (error) {
			console.log(error);
			alert(error.response.data.info);
		} finally {
			setLoading(false);
		}
	};

	return (
		<div className="form">
			<h2 className="text-2xl font-bold mb-6 text-center">Login Form</h2>
			<Formik onSubmit={onSubmit} initialValues={initialState} validationSchema={validationSchema}>
				{params => {
					return (
						<form className="form-input">
							<div className="form-input-group">
								<label>Email:</label>
								<input
									type="text"
									value={params.values.email}
									onChange={params.handleChange}
									onBlur={params.handleBlur}
									name="email"
									placeholder="Masukkan Email"
									className="border-2 border-gray-500 rounded-sm px-2"
								/>
								<div></div>
								{params.touched.email && params.errors.email && (
									<div className="error-message">{params.errors.email}</div>
								)}
							</div>
							<div className="form-input-group">
								<label>Password:</label>
								<input
									type="password"
									value={params.values.password}
									onChange={params.handleChange}
									onBlur={params.handleBlur}
									name="password"
									placeholder="Masukkan Password"
									className="border-2 border-gray-500 rounded-sm px-2"
								/>
								<div></div>
								{params.touched.password && params.errors.password && (
									<div className="error-message">{params.errors.password}</div>
								)}
							</div>
							<div className="flex justify-center">
								<button
									type="button"
									className={`px-4 py-2 border rounded-md ${
										params.isValid && params.dirty ? 'border-green-200 bg-green-200' : 'border-gray-400 bg-gray-400'
									} `}
									onClick={params.handleSubmit}
									disabled={loading}
								>
									{loading === true ? 'Loading...' : 'Login'}
								</button>
							</div>
						</form>
					);
				}}
			</Formik>
		</div>
	);
}

export default LoginForm;
