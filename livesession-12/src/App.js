import React, { useContext, useEffect } from 'react';
import './App.css';
import ArticleContainer from './components/ArticleContainer';
import Form from './components/Form';
import TableArticle from './components/TableArticle';
import UpdateForm from './components/UpdateForm';
import { GlobalContext } from './context/GlobalContext';

function App() {
	const { count, setCounter, edit, fetchArticles, loading } = useContext(GlobalContext);

	useEffect(() => {
		fetchArticles();
	}, []);

	return (
		<div>
			<div style={{ display: 'flex', justifyContent: 'center' }}>
				<button onClick={() => setCounter(count - 1)}>Minus</button>
				<button onClick={() => setCounter(count + 1)}>Plus</button>
			</div>
			{edit === false ? <Form /> : <UpdateForm />}

			{loading === true ? (
				<h1 style={{ textAlign: 'center' }}>Loading....</h1>
			) : (
				<>
					<TableArticle />
					<ArticleContainer />
				</>
			)}
		</div>
	);
}

export default App;
