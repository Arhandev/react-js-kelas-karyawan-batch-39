import React from 'react';

function Article({ item }) {
	return (
		<div className="article">
			<div className="image-container">
				<img src={item.image_url} alt="" />
			</div>
			<div className="content-container">
				<h1>{item.judul}</h1>
				<p>{item.konten}</p>
			</div>
		</div>
	);
}

export default Article;
