import axios from 'axios';
import React, { useContext, useEffect, useState } from 'react';
import { GlobalContext } from '../context/GlobalContext';

function UpdateForm() {
	const { edit, setEdit, fetchArticles } = useContext(GlobalContext);
	const [input, setInput] = useState({
		judul: '',
		konten: '',
		image_url: '',
	});
	const [loading, setLoading] = useState(false);
	const onChangeInput = event => {
		setInput({ ...input, [event.target.name]: event.target.value });
	};

	useEffect(() => {
		setInput({
			judul: edit.judul,
			konten: edit.konten,
			image_url: edit.image_url,
		});
	}, []);

	const onUpdate = async () => {
		setLoading(true);
		try {
			const response = await axios.put(`https://arhandev.xyz/public/api/articles/${edit.id}`, {
				judul: input.judul,
				konten: input.konten,
				image_url: input.image_url,
			});
			alert('Berhasil update');
			fetchArticles();
			setInput({
				judul: '',
				konten: '',
				image_url: '',
			});
			setEdit(false);
		} catch (error) {
			console.log(error);
			alert('Terjadi Kesalahan');
		} finally {
			setLoading(false);
		}
	};

	return (
		<div className="form">
			<h2 style={{ textAlign: 'center' }}>Update Form</h2>
			<form className="form-input">
				<div className="form-input-group">
					<label>Judul:</label>
					<input type="text" value={input.judul} onChange={onChangeInput} name="judul" placeholder="Masukkan judul" />
				</div>
				<div className="form-input-group">
					<label>Konten:</label>
					<input
						type="text"
						value={input.konten}
						onChange={onChangeInput}
						placeholder="Masukkan konten"
						name="konten"
					/>
				</div>
				<div className="form-input-group">
					<label>Image Url:</label>
					<input
						type="text"
						value={input.image_url}
						onChange={onChangeInput}
						placeholder="Masukkan link gambar"
						name="image_url"
					/>
				</div>
				<div className="button-container">
					<button type="button" disabled={loading} onClick={onUpdate}>
						{loading === true ? 'Loading...' : 'Update'}
					</button>
				</div>
			</form>
		</div>
	);
}

export default UpdateForm;
