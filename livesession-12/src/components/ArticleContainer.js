import React, { useContext } from 'react';
import { GlobalContext } from '../context/GlobalContext';
import Article from './Article';

function ArticleContainer() {
	const { count, data } = useContext(GlobalContext);

	return (
		<div className="article-container">
			<h1>{count}</h1>
			<h1>Preview Artikel</h1>
			{data.map((item, index) => {
				return <Article item={item} />;
			})}
		</div>
	);
}

export default ArticleContainer;
