import axios from 'axios';
import React, { useContext } from 'react';
import { GlobalContext } from '../context/GlobalContext';

function TableArticle() {
	const { count, data, setEdit, fetchArticles } = useContext(GlobalContext);
	const onDelete = async article => {
		try {
			const response = await axios.delete(`https://arhandev.xyz/public/api/articles/${article.id}`);
			alert('Berhasil Delete');
			fetchArticles();
		} catch (error) {
			console.log(error);
			alert('Terjadi Suatu Error');
		}
	};

	const handleEdit = article => {
		setEdit(article);
	};
	return (
		<div>
			<h1 style={{ textAlign: 'center' }}>{count}</h1>
			<h1 style={{ textAlign: 'center' }}>Tabel Pengaturan Artikel</h1>
			<table>
				<thead>
					<tr>
						<th>Gambar</th>
						<th>Judul</th>
						<th>Konten</th>
						<th>Action</th>
					</tr>
				</thead>
				<tbody>
					{data.map(article => {
						return (
							<tr>
								<td>
									<img src={article.image_url} width="200" alt="" />
								</td>
								<td>{article.judul}</td>
								<td>{article.konten}</td>
								<td>
									<div className="action">
										<button onClick={() => handleEdit(article)} className="btn btn-edit">
											Edit
										</button>
										<button onClick={() => onDelete(article)} className="btn btn-delete">
											Delete
										</button>
									</div>
								</td>
							</tr>
						);
					})}
				</tbody>
			</table>
		</div>
	);
}

export default TableArticle;
