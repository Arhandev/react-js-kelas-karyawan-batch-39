import React from 'react';
import SmallComponent from './SmallComponent';

function Test() {
	const nama = 'Aan1234';
	function onCancel() {
		alert('Berhasil di cancel');
	}
	function onSubmit() {
		alert('Berhasil disubmit');
	}

	const arr = ['Value 1', 'Value 2', 'Value 3'];

	return (
		<div>
			<h1>{nama} = ini adalah value variabel nama</h1>
			<button onClick={onCancel}>cancel</button>
			<button onClick={onSubmit}>submit</button>
			{/* {arr.map((item, index) => {
				return <SmallComponent judul={item}/>
			})} */}
			<SmallComponent judul={12 > 10 ? 'Pernyataan benar' : 'Pernyataan salah'} />
		</div>
	);
}

export default Test;
