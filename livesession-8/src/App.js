import './App.css';
import TestComponent from './Test';

function App() {
	return (
		<div>
			<h1>Ini adalah Text dari App Component</h1>
			<TestComponent />
		</div>
	);
}

export default App;
